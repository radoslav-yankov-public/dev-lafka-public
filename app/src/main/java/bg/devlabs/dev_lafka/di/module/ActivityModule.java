package bg.devlabs.dev_lafka.di.module;

import android.app.Activity;
import android.content.Context;

import bg.devlabs.dev_lafka.di.ActivityContext;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Radoslav on 06-Mar-17.
 */


@Module
public class ActivityModule {

	private Activity mActivity;

	public ActivityModule(Activity activity) {
		this.mActivity = activity;
	}

	@Provides
	@ActivityContext
	Context provideContext() {
		return mActivity;
	}

	@Provides
	Activity provideActivity() {
		return mActivity;
	}

	@Provides
	CompositeDisposable provideCompositeDisposable() {
		return new CompositeDisposable();
	}



}
