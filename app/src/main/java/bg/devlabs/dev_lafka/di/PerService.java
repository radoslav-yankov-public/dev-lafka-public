package bg.devlabs.dev_lafka.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerService {
}

