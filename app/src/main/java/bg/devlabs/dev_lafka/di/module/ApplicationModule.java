package bg.devlabs.dev_lafka.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import bg.devlabs.dev_lafka.BuildConfig;
import bg.devlabs.dev_lafka.data.AppDataManager;
import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.data.network.ApiHelper;
import bg.devlabs.dev_lafka.data.network.AppApiHelper;
import bg.devlabs.dev_lafka.data.prefs.AppPreferencesHelper;
import bg.devlabs.dev_lafka.data.prefs.PreferencesHelper;
import bg.devlabs.dev_lafka.di.ApplicationContext;
import bg.devlabs.dev_lafka.di.PreferenceInfo;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Radoslav on 06-Mar-17.
 */


@Module
public class ApplicationModule {

	private final Application mApplication;

	public ApplicationModule(Application application) {
		mApplication = application;
	}

	@Provides
	@ApplicationContext
	Context provideContext() {
		return mApplication;
	}

	@Provides
	Application provideApplication() {
		return mApplication;
	}

	@Provides
	@Singleton
	Cache provideHttpCache(Application application) {
		int cacheSize = 10 * 1024 * 1024;
		return new Cache(application.getCacheDir(), cacheSize);
	}

	@Provides
	@Singleton
	Gson provideGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
		return gsonBuilder.create();
	}

	@Provides
	@Singleton
	OkHttpClient provideOkhttpClient(Cache cache) {
		OkHttpClient.Builder client = new OkHttpClient.Builder();
		client.cache(cache);
		return client.build();
	}

	@Provides
	@Singleton
	Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
		return new Retrofit.Builder()
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(gson))
				.baseUrl(BuildConfig.BASE_URL)
				.client(okHttpClient)
				.build();
	}

	@Provides
	@PreferenceInfo
	String providePreferenceName() {
		return "lafka_pref";
	}

	@Provides
	@Singleton
	DataManager provideDataManager(AppDataManager appDataManager) {
		return appDataManager;
	}


	@Provides
	@Singleton
	ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
		return appApiHelper;
	}

	@Provides
	@Singleton
	PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
		return appPreferencesHelper;
	}

	@Provides
	CompositeDisposable provideCompositeDisposable() {
		return new CompositeDisposable();
	}

//	@Provides
//	@PerActivity
//	Activity provideActivity(Activity activity){
//		return null;
//	}

//	@Provides
//	@Singleton
//	LoginPresenter<LoginView> provideLoginPresenter(LoginPresenter<LoginView>
//															presenter) {
//		return presenter;
//	}
//
//	@Provides
//	@Singleton
//	HomePresenter<HomeView> provideHomePresenter(HomePresenter<HomeView>
//														 presenter) {
//		return presenter;
//	}




}
