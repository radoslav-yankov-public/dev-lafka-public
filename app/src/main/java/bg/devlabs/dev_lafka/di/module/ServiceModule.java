package bg.devlabs.dev_lafka.di.module;

import android.app.Service;

import dagger.Module;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@Module
public class ServiceModule {

	private final Service mService;

	public ServiceModule(Service service) {
		mService = service;
	}
}
