package bg.devlabs.dev_lafka.data.prefs;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public interface PreferencesHelper {

	/**
	 * Gets the user's last name
	 * @return a string, last name
	 */
	String getCurrentUserName();

	/**
	 * Sets the user's last name
	 * @param name User's last name. Extracted from the login call.
	 */
	void setCurrentUserName(String name);

	/**
	 * Gets the user's session
	 * @return a string, user's session
	 */
	String getAccessToken();

	/**
	 * Sets the user's session
	 * @param accessToken User's session. Extracted from the login call.
	 */
	void setAccessToken(String accessToken);

	/**
	 * Gets the user's last item bought
	 * @return a string, user's last item bought
	 */
	String getLastItem();

	/**
	 * Sets the last item bought
	 * @param item User's last item bought.
	 */
	void setLastItem(String item);

}
