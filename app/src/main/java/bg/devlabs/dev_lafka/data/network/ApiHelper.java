package bg.devlabs.dev_lafka.data.network;

import java.util.List;

import bg.devlabs.dev_lafka.data.network.format.ApiResponse;
import bg.devlabs.dev_lafka.data.network.model.LogResponse;
import bg.devlabs.dev_lafka.data.network.model.MakeResponse;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import io.reactivex.Observable;

/**
 * Created by Radoslav on 06-Mar-17.
 * Handler of API calls
 */

public interface ApiHelper {

	/**
	 * 	Login call
	 * @param email user's email adress
	 * @param password user's password
	 * @return an observable with the full JSON file including errors and data
	 */
	Observable<ApiResponse<LogResponse>> doLoginCall(String email, String password);


	/**
	 * Make an order call
	 * @param session_id sessionId
	 * @param productId the unique ID of the lafka product
	 * @param quantity the quantity of the lafka product. Currently always set at 1
	 * @return an observable with the full JSON file including errors and data
	 */
	Observable<ApiResponse<MakeResponse>> doMakeCall(String session_id, int productId, int quantity);

	/**
	 * 	Logout call
	 * @param session_id sessionId
	 * @return an observable with the success state of the call
	 */
	Observable<ApiResponse> doLogoutCall(String session_id);

	/**
	 * Call for all of lafka's products
	 * @param session_id sessionId
	 * @return an observable with the full JSON file including errors and data
	 */
	Observable<ApiResponse<List<ProductsResponse>>> doProductsCall(String session_id);

	/**
	 * 	Call for all of the current user's orders
	 * @param session_id sessionId
	 * @param lastUpdated last time the local database was updated. Currently not implemented
	 * @return an observable with the full JSON file including errors and data
	 */
	Observable<ApiResponse<List<OrdersResponse>>> doOrdersCall(String session_id, String lastUpdated);

	/**
	 * Call for the deletion of an item from the user's orders
	 * @param session_id sessionId
	 * @param id the ID of the lafka ORDER
	 * @return an observable with the success state of the call
	 */
	Observable<ApiResponse> doDeleteCall(String session_id, int id);

}
