package bg.devlabs.dev_lafka.data.network;

import java.util.List;

import bg.devlabs.dev_lafka.data.network.format.ApiResponse;
import bg.devlabs.dev_lafka.data.network.model.LogResponse;
import bg.devlabs.dev_lafka.data.network.model.MakeResponse;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import io.reactivex.Observable;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Radoslav on 06-Mar-17.
 * API endpoints
 */

public interface TaskService {
	@GET("/api/v1.0/lafka_meals")
	Observable<ApiResponse<List<ProductsResponse>>> getProducts(
			@Header("lafka_cookie") String sessionId);

	@GET("/api/v1.0/users/lafka_orders")
	Observable<ApiResponse<List<OrdersResponse>>> getUsersOrders(
			@Header("lafka_cookie") String sessionId,
			@Query("last_update") String lastUpdate);

	@FormUrlEncoded
	@POST("/api/v1.0/make_order")
	Observable<ApiResponse<MakeResponse>> makeOrder(
			@Header("lafka_cookie") String sessionId,
			@Field("product_id") int product_id,
			@Field("quantity") int quantity);

	@DELETE("/api/v1.0/order/{id}")
	Observable<ApiResponse> deleteOrder(
			@Header("lafka_cookie") String sessionId,
			@Path("id") int order_id);

	@FormUrlEncoded
	@POST("/api/v1.0/login")
	Observable<ApiResponse<LogResponse>> login(
			@Field("email") String email,
			@Field("password") String password);

	@GET("/api/v1.0/logout")
	Observable<ApiResponse> logout(
			@Header("lafka_cookie") String sessionId);
}