package bg.devlabs.dev_lafka.data.network.format;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public class ApiResponse <T>{
	boolean success;
	T data;
	String error;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
