package bg.devlabs.dev_lafka.data;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.network.ApiHelper;
import bg.devlabs.dev_lafka.data.network.format.ApiResponse;
import bg.devlabs.dev_lafka.data.network.model.LogResponse;
import bg.devlabs.dev_lafka.data.network.model.MakeResponse;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.data.prefs.PreferencesHelper;
import bg.devlabs.dev_lafka.di.ApplicationContext;
import io.reactivex.Observable;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public class AppDataManager implements DataManager{

	private final Context mContext;
	private final PreferencesHelper mPreferencesHelper;
	private final ApiHelper mApiHelper;

	@Inject
	public AppDataManager(@ApplicationContext Context context,
						  PreferencesHelper preferencesHelper,
						  ApiHelper apiHelper) {
		mContext = context;
		mPreferencesHelper = preferencesHelper;
		mApiHelper = apiHelper;
	}


	@Override
	public Observable<ApiResponse<LogResponse>> doLoginCall(String email, String password) {
		return mApiHelper.doLoginCall(email,password);
	}

	@Override
	public Observable<ApiResponse<MakeResponse>> doMakeCall(String accessToken, int productId, int quantity) {
		return mApiHelper.doMakeCall(accessToken, productId, quantity);
	}

	@Override
	public Observable<ApiResponse> doLogoutCall(String accessToken) {
		return mApiHelper.doLogoutCall(accessToken);
	}

	@Override
	public Observable<ApiResponse<List<ProductsResponse>>> doProductsCall(String accessToken) {
		return mApiHelper.doProductsCall(accessToken);
	}

	@Override
	public Observable<ApiResponse<List<OrdersResponse>>> doOrdersCall(String accessToken, String lastUpdated) {
		return mApiHelper.doOrdersCall(accessToken, lastUpdated);
	}

	@Override
	public Observable<ApiResponse> doDeleteCall(String accessToken, int id) {
		return mApiHelper.doDeleteCall(accessToken, id);
	}

	@Override
	public String getCurrentUserName() {
		return mPreferencesHelper.getCurrentUserName();
	}

	@Override
	public void setCurrentUserName(String name) {
		mPreferencesHelper.setCurrentUserName(name);

	}

	@Override
	public String getAccessToken() {
		return mPreferencesHelper.getAccessToken();
	}

	@Override
	public void setAccessToken(String accessToken) {
		mPreferencesHelper.setAccessToken(accessToken);
	}

	@Override
	public String getLastItem() {
		return mPreferencesHelper.getLastItem();
	}

	@Override
	public void setLastItem(String item) {
		mPreferencesHelper.setLastItem(item);
	}

	@Override
	public void updateUserInfo(String accessToken, String name) {
		mPreferencesHelper.setAccessToken(accessToken);
		mPreferencesHelper.setCurrentUserName(name);
	}
}
