package bg.devlabs.dev_lafka.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public class ApiError {

	@Expose
	@SerializedName("success")
	private Boolean statusCode;

	@Expose
	@SerializedName("error")
	private String message;

	public ApiError(Boolean statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}


	public Boolean getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Boolean statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
