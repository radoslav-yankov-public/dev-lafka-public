package bg.devlabs.dev_lafka.utils;

import android.os.Handler;

/**
 * Created by Radoslav on 17-Mar-17.
 *
 * Wrapper for delayed handlers
 */

public class Not {

	public static void now(Runnable r){
		now(200,r);
	}

	public static void now(int seconds, Runnable r){
		new Handler().postDelayed(r, seconds);
	}

}
