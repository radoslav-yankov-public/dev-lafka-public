package bg.devlabs.dev_lafka.utils;

/**
 * Created by Radoslav on 08-Mar-17.
 *
 *	Different Auth Utilities
 */

public class AuthUtils {
	/**
	 * Checks if the user's email is valid
	 * @param target User's email
	 * @return a boolean
	 */
	public static final boolean isValidEmail(CharSequence target) {
		return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	/**
	 * Checks if the user's pass is valid
	 * @param target User's pass
	 * @return a boolean
	 */
	public static final boolean isValidPass(String target){
		return target.length() > 4;
	}
}
