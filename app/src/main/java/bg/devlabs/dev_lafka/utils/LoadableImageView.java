package bg.devlabs.dev_lafka.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import bg.devlabs.dev_lafka.R;

/**
 * Created by Radoslav on 10-Mar-17.
 *
 * ImageView for dynamically loading images from the internet. Using Picasso
 */

public class LoadableImageView extends ImageView {
	private static final String LINK_TO_PRODUCT_PICTURES = "http://devboard.devlabs-projects.com/img/lafka/";
	public LoadableImageView(Context context) {
		super(context);
	}

	public LoadableImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LoadableImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public LoadableImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	public void load(String fileName) {
		String imageUrl;
			String fileNamePlusFormat = fileName + ".png";
			imageUrl = LINK_TO_PRODUCT_PICTURES + fileNamePlusFormat;
			Log.d("imageUrl", imageUrl);
			Picasso.with(super.getContext())
					.load(imageUrl)
					.error(getResources().getDrawable(R.drawable.lafka_product_default))
					.into(this);

	}

}