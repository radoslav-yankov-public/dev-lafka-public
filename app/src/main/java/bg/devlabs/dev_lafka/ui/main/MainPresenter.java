package bg.devlabs.dev_lafka.ui.main;

import bg.devlabs.dev_lafka.ui.base.LafkaPresenter;

/**
 * Created by Radoslav on 10-Mar-17.
 */
public interface MainPresenter<V extends MainView> extends LafkaPresenter<V> {
	/**
	 * Check the validity of the login
	 */
	void checkLogin();

	/**
	 * Load a list of all of lafka's products after utils/ProductsList
	 */
	void loadProducts();
}
