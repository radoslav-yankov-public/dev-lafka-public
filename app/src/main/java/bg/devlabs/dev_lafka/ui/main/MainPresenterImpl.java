package bg.devlabs.dev_lafka.ui.main;

import com.androidnetworking.error.ANError;

import java.util.Collections;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.ui.base.BasePresenter;
import bg.devlabs.dev_lafka.ui.main.MainPresenter;
import bg.devlabs.dev_lafka.utils.ProductsList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Radoslav on 10-Mar-17.
 *
 * Main Activity Logic
 */

public class MainPresenterImpl<V extends MainView> extends BasePresenter<V> implements MainPresenter<V> {

	@Inject
	public MainPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
		super(dataManager, compositeDisposable);
	}

	/**
	 * Checks whether user is logged after
	 */
	@Override
	public void checkLogin() {
		if (getDataManager().getAccessToken().equals("")) {
			//loads all of lafka's products and shows login on null session
			loadProducts();
			getLafkaView().showLoginFragment(false);
		} else {
			{
				//checks whether user is logged after. This is the only call that checks for user login without making a order or deleting an item....
				getDataManager().doOrdersCall(getDataManager().getAccessToken(), "")
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(listApiResponse-> {
							if (listApiResponse.isSuccess()){
								//shows home on success
								getLafkaView().showHomeFragment(false);
							} else {
								//shows login on fail
								getLafkaView().showLoginFragment(false);
							}
							loadProducts();
						}, throwable -> handleApiError(new ANError(throwable)));
			}
		}
	}

	/**
	 * Loads products into utils/ProductsList
	 */
	@Override
	public void loadProducts() {
				getDataManager().doProductsCall(getDataManager().getAccessToken())
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(listApiResponse -> {
					if (listApiResponse.isSuccess()) {
						//sorts items based on quantity and availability
						Collections.sort(listApiResponse.getData(), (lhs, rhs) -> Integer.parseInt(lhs.getQuantity()) >  Integer.parseInt(rhs.getQuantity())
								? -1 :  Integer.parseInt(lhs.getQuantity()) <  Integer.parseInt(rhs.getQuantity()) ? 1 : 0);
						ProductsList.products = listApiResponse.getData();
					} else {
						getLafkaView().onError(R.string.api_default_error);
					}
				}, throwable -> handleApiError(new ANError(throwable)));
	}
}
