package bg.devlabs.dev_lafka.ui.main;


import bg.devlabs.dev_lafka.ui.base.LafkaView;

/**
 * Created by Radoslav on 10-Mar-17.
 */
public interface MainView extends LafkaView {
	/**
	 * Shows the main Home fragment
	 * @param shouldAnimate Whether it should be shown with an animation
	 */
	void showHomeFragment(boolean shouldAnimate);
	/**
	 * Shows the Login fragment
	 * @param shouldAnimate Whether it should be shown with an animation
	 */
	void showLoginFragment(boolean shouldAnimate);

	/**
	 * Shows the List fragment
	 * @param isBuyScreen Whether it should show the full products list or the user's order list
	 */
	void showBillFragment(boolean isBuyScreen);

	/**
	 * Opens up the main Home fragment with an already selected item
	 * @param barcode the selected item's barcode
	 */
	void openHomeWithSelected(String barcode);
}
