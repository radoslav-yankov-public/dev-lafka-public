package bg.devlabs.dev_lafka.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.Result;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.ui.base.BaseFragment;
import bg.devlabs.dev_lafka.ui.main.MainActivity;
import bg.devlabs.dev_lafka.ui.shortcut.ShortcutActivity;
import bg.devlabs.dev_lafka.utils.LoadableImageView;
import bg.devlabs.dev_lafka.utils.Not;
import bg.devlabs.dev_lafka.utils.ShakeDetector;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public class HomeFragment extends BaseFragment implements HomeView, ZXingScannerView.ResultHandler {
	public static final	int SOUND_ARR[] = {R.raw.bar1, R.raw.bar2, R.raw.bar3, R.raw.bar4, R.raw.bar5, R.raw.bar6, R.raw.bar7, R.raw.bar8, R.raw.bar9, R.raw.bar10};

	@Inject
	HomePresenterImpl<HomeView> mPresenter;

	@BindView(R.id.view_scan)
	FrameLayout scanView;
	@BindView(R.id.image_logo)
	LoadableImageView logo;
	@BindView(R.id.logout)
	ImageView logout;
	@BindView(R.id.text_item)
	TextView textItem;
	@BindView(R.id.text_price)
	TextView textPrice;
	@BindView(R.id.text_barcode)
	TextView textBarcode;
	@BindView(R.id.text_bill)
	TextView textBill;
	@BindView(R.id.button_action)
	FloatingActionButton buttonAction;
	@BindView(R.id.home_card)
	CardView card;
	@BindView(R.id.layout_home)
	RelativeLayout layoutHome;

	//barcode scanner
	private ZXingScannerView mScannerView;
	//is camera currently on
	boolean isCameraOn = false;
	//id and barcode of currently selected item
//	String mId, mBacrode;
	ProductsResponse currentlySelected;
	//shake related variables
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private ShakeDetector mShakeDetector;

	public static HomeFragment newInstance() {
		return new HomeFragment();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.card_home, container, false);
		ButterKnife.bind(this, rootView);
		Devlafka.getAppComponent(getActivity()).inject(this);
		mPresenter.onAttach(this);
		setUp(rootView);
		return rootView;
	}

	@Override
	public void setUp(View view) {
		setupProducts();
		setupScanner();
		setupShaker();
		updateAutomatic();
	}


	/**
	 * Shake mostion setup
	 */
	private void setupShaker() {
		mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mShakeDetector = new ShakeDetector();
		mShakeDetector.setOnShakeListener(listener);
	}

	/**
	 * Shows an error/success after purchase attempt
	 *
	 * @param state error/success
	 */
	private void setupError(boolean state) {
		buttonAction.show();
		buttonAction.setClickable(true);
		setError(card, state);
	}

	/**
	 * Sets all lafka products
	 */
	private void setupProducts() {
		mPresenter.setProducts();
	}

	/**
	 * Barcode scanner setup
	 */
	private void setupScanner() {
		mScannerView = new ZXingScannerView(getContext()) {
			@Override
			protected IViewFinder createViewFinderView(Context context) {
				return new bg.devlabs.dev_lafka.utils.CustomViewFinderView(context);
			}
		};
		mScannerView.setResultHandler(this);
		mScannerView.setAlpha(0.0f);
		scanView.addView(mScannerView);
	}

	@OnClick(R.id.text_barcode)
	public void onClick() {
		mPresenter.onBarcodeClicked();
		//checks for camera permissions
		requestPermission(Manifest.permission.CAMERA, msg -> {
			if (msg.getData().getBoolean("permission")) {
				toggleCamera();
			}
			return false;
		});
	}

	/**
	 * Attempts a purchase with give product ID
	 */
	@OnClick(R.id.button_action)
	public void onClick2() {
		if (currentlySelected != null) {
		buttonAction.setClickable(false);
			mPresenter.purchase(currentlySelected.getId());
		}
	}

	/**
	 * Shows bill fragment and sets up animations and animations and animation directions
	 */
	@OnClick(R.id.text_bill)
	public void onClick3() {
		direction = 3;
		shouldAnimate = true;
		buttonAction.hide();
		((MainActivity) getActivity()).showBillFragment(false);
	}

	/**
	 * Shows all lafka orders fragment and sets up animations and animations and animation directions
	 */
	@OnClick(R.id.image_logo)
	public void onClick4() {
		direction = 2;
		shouldAnimate = true;
		((MainActivity) getActivity()).showBillFragment(true);
	}


	/**
	 * Logout
	 */
	@OnClick(R.id.logout)
	public void onClick5() {
		mPresenter.logout();
	}

	/**
	 * Sets the direction animation of the fragment
	 *
	 * @param direction direction....
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}

	/**
	 * Shows an error
	 */
	@Override
	public void showError() {
		setupError(true);
	}

	/**
	 * Shows a success
	 */
	@Override
	public void showSuccess() {
		setupError(false);
	}

	/**
	 * Sets a selected item into the view
	 *
	 * @param response selected item's Object
	 */
	@Override
	public void setSelected(ProductsResponse response) {
		currentlySelected = response;
	}

	public void showSelected(ProductsResponse response) {
		buttonAction.show();
		//sets up and animated texts and images
		logo.load(response.getBarcode());
//		mId = response.getId();
		textItem.animate().alpha(0.0f).setDuration(200);
		textPrice.animate().alpha(0.0f).setDuration(200);

		Not.now(500, () -> {
			textItem.setText(response.getName());
			textPrice.setText(response.getPrice() + getString(R.string.lv));
			textItem.animate().alpha(1.0f).setDuration(200);
			textPrice.animate().alpha(1.0f).setDuration(200);
			if (response.getQuantity().equals("0"))
				textItem.setTextColor(getActivity().getResources().getColor(R.color.red));
			else {
				textItem.setTextColor(getActivity().getResources().getColor(R.color.grey));
			}
		});
	}

	@Override
	public void openHomeScreen() {
		direction = 4;
		shouldAnimate = true;
		((MainActivity) getActivity()).showLoginFragment(true);
	}

	@Override
	public void onShake(String name) {
		//gets a random shake message
		int randomNum = new Random().nextInt(5);
		if (randomNum == 4) {
			onError(getResources().getString(R.string.shake5, name));
		} else {
			String[] planets = getResources().getStringArray(R.array.shake);
			onError(planets[randomNum]);

		}
	}

	@Override
	public void onError(String message) {
		buttonAction.setClickable(true);
		//checks whether error is invalid user
		if (message.equals("User not found, please login again")) {
			((MainActivity) getActivity()).showLoginFragment(true);
		}
		super.onError(message);
	}

	/**
	 * Toggles barcode scanner camera
	 */
	@Override
	public void toggleCamera() {
		if (!isCameraOn) {
			mShakeDetector.setOnShakeListener(null);
			mScannerView.setResultHandler(this);
			mScannerView.startCamera();
			mScannerView.setAlpha(1.0f);
		} else {
			stopCamera();
		}
		isCameraOn = !isCameraOn;
	}

	public void stopCamera() {
//		mShakeDetector.setOnShakeListener(listener);
		mScannerView.stopCamera();
		mScannerView.setAlpha(0.0f);
	}

	/**
	 * Updates bill with given number
	 *
	 * @param bill The bill
	 */
	@Override
	public void updateBill(double bill) {
		String cleanBill = String.valueOf(bill);
		if (cleanBill.length() > 4) {
			cleanBill = cleanBill.substring(0, 4);
		}
		textBill.setText(cleanBill + "лв");
		if (bill <= 0) {
			textBill.setClickable(false);
			textBill.setAlpha(0.0f);
		} else {
			textBill.setClickable(true);
			textBill.animate().alpha(1.0f).setDuration(400);
		}
	}

	/**
	 * Sets up screen with an already selected from another screen item
	 *
	 * @param id the items ID
	 */
	@Override
	public void openWithSelected(String id) {
		mPresenter.setSelected(id);
	}

	@Override
	@RequiresApi(api = Build.VERSION_CODES.N_MR1)
	public void setupIconShortcut(String text) {
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.N) {
			ShortcutManager shortcutManager;
			String shortText = text;
			if (text.length() > 10) {
				shortText = text.substring(0, 10);
			}
			shortcutManager = getActivity().getSystemService(ShortcutManager.class);
			ShortcutInfo shortcut = new ShortcutInfo.Builder(getActivity(), "id1")
					.setShortLabel("Buy " + shortText)
					.setLongLabel("Buy " + text)
					.setIcon(Icon.createWithResource(getActivity(), R.drawable.shortcut))
					.setIntent(new Intent(Intent.ACTION_MAIN, Uri.EMPTY, getActivity(), ShortcutActivity.class))
					.build();
			List<ShortcutInfo> list = shortcutManager.getDynamicShortcuts();
			list.add(shortcut);
			shortcutManager.setDynamicShortcuts(list);
		}

	}


	/**
	 * Handles the result from the barcode scanner
	 *
	 * @param result the result object
	 */
	@Override
	public void handleResult(Result result) {
		if (result != null) {
			mPresenter.setSelected(result.getText());
			updateAutomatic();
			playBarcodeSound();
		} else {
			showError();
		}
		isCameraOn = false;
		stopCamera();
	}

	private void playBarcodeSound() {
		int randomNum = new Random().nextInt(10);
        final MediaPlayer mp = MediaPlayer.create(getContext(), SOUND_ARR[randomNum]);
		mp.start();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		buttonAction.show();
		mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	public void onPause() {
		mSensorManager.unregisterListener(mShakeDetector);
		buttonAction.hide();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * Automatically updates user's bill
	 */
	@Override
	public void updateAutomatic() {
		textBill.setAlpha(0.0f);
		mPresenter.loadBill();
		if (currentlySelected != null) {
			showSelected(currentlySelected);
		}
	}

	ShakeDetector.OnShakeListener listener = new ShakeDetector.OnShakeListener() {
		@Override
		public void onShake(int count) {

			mPresenter.onShake();
		}
	};


	@Override
	public void showViews() {
		layoutHome.setAlpha(1.0f);
	}


}
