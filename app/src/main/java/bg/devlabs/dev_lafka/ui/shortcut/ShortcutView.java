package bg.devlabs.dev_lafka.ui.shortcut;

import bg.devlabs.dev_lafka.ui.base.LafkaView;

/**
 * Created by Radoslav on 17-Mar-17.
 */

public interface ShortcutView extends LafkaView {
	public void showToast(boolean isSuccess);
}
