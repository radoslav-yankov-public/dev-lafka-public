package bg.devlabs.dev_lafka.ui.home;

import bg.devlabs.dev_lafka.di.PerActivity;
import bg.devlabs.dev_lafka.ui.base.LafkaPresenter;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@PerActivity
public interface HomePresenter<V extends HomeView> extends LafkaPresenter<V> {
	/**
	 * Attempts to purchase an item
	 * @param id item's ID
	 */
	void purchase(String id);

	/**
	 * Sets the list with the lafka's full products list
	 */
	void setProducts();

	/**
	 * Selects an item based on an ID
	 * @param id item's ID
	 */
	void setSelected(String id);

	/**
	 * Sets up the barcode scanner
	 */
	void onBarcodeClicked();
	void loadProducts();

	/**
	 * Loads the user's bill
	 */
	void loadBill();

	/**
	 * Handles phone shakes
	 */
	void onShake();


	/**
	 * Attempts a logout
	 */
	void logout();
}
