package bg.devlabs.dev_lafka.ui.base;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;

import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.di.component.ApplicationComponent;
import bg.devlabs.dev_lafka.utils.FlipAnimation;
import bg.devlabs.dev_lafka.utils.Not;
import butterknife.Unbinder;

/**
 * Created by Radoslav on 08-Mar-17.
 *
 * Base Fragment
 */

public abstract class BaseFragment extends Fragment implements LafkaView {

	private BaseActivity mActivity;
	private Unbinder mUnBinder;
	protected boolean shouldAnimate = true;
	protected int direction = 3;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof BaseActivity) {
			BaseActivity activity = (BaseActivity) context;
			this.mActivity = activity;
			activity.onFragmentAttached();
		}
	}


	/**
	 * 	Sets whether the fragment should be animated on entry/exit
	 * @param shouldAnimate boolean for whether fragment should be animated
	 * @param direction which direction should it be animated
	 *                  1 - UP
	 *                  2 - DOWN
	 *                  3 - LEFT
	 *                  4 - RIGHT
	 */
	public void setShouldAnimate(boolean shouldAnimate, int direction) {
		this.shouldAnimate = shouldAnimate;
		this.direction = direction;
	}

	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		if (shouldAnimate) {
			return FlipAnimation.create(direction, enter, 650);
		} else {
			return super.onCreateAnimation(transit, enter, nextAnim);
		}
	}

	/**
	 * Opens up an activity on expired token
	 */
	@Override
	public void openActivityOnTokenExpire() {
		if (mActivity != null) {
			mActivity.openActivityOnTokenExpire();
		}
	}

	/**
	 * Shows error with provided string ID
	 * @param resId string ID
	 */
	@Override
	public void onError(@StringRes int resId) {
		if (mActivity != null) {
			mActivity.onError(resId);
		}
	}

	/**
	 * Shows error with provided string
	 * @param message string
	 */
	@Override
	public void onError(String message) {
		if (mActivity != null) {
			mActivity.onError(message);
		}
	}

	@Override
	public boolean isNetworkConnected() {
		return mActivity != null && mActivity.isNetworkConnected();
	}

	@Override
	public void hideKeyboard() {
		if (mActivity != null) {
			mActivity.hideKeyboard();
		}
	}

	/**
	 * 	Requests permission. Used (currently) only for camera permission
	 * @param permission the permission string
	 * @param callback for handling the user's action
	 */
	@Override
	public void requestPermission(String permission, Handler.Callback callback){
		mActivity.requestPermission(permission, callback);
	}


//	public ActivityComponent getActivityComponent() {
//		return mActivity.getActivityComponent();
//	}

	/**
	 * Setting the card's background color
	 * @param card The card to be set
	 * @param isError Whether an error should be shown or a success
	 */
	protected void setError(CardView card, boolean isError){
		final ColorStateList originalColor = card.getCardBackgroundColor();
		int fullDuration = isError?220:330;
		int colorFrom = originalColor.getDefaultColor();
		int colorTo = isError?getResources().getColor(R.color.light_red): getResources().getColor(R.color.green);
		ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
		colorAnimation.setDuration(fullDuration);
		colorAnimation.addUpdateListener(animator -> card.setCardBackgroundColor((int) animator.getAnimatedValue()));
		colorAnimation.start();

		Not.now(fullDuration + 20, () -> {
			ValueAnimator colorAnimationEnd = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
			colorAnimationEnd.addUpdateListener(animator -> card.setCardBackgroundColor((int) animator.getAnimatedValue()));
			colorAnimationEnd.setDuration(fullDuration*2);
			colorAnimationEnd.start();
		});
	}


	public ApplicationComponent getApplicationComponent(){
		return mActivity.getmApplicationComponent();
	}


	public BaseActivity getBaseActivity() {
		return mActivity;
	}

	public void setUnBinder(Unbinder unBinder) {
		mUnBinder = unBinder;
	}

	protected abstract void setUp(View view);

	public interface Callback {

		void onFragmentAttached();

		void onFragmentDetached(String tag);
	}
}
