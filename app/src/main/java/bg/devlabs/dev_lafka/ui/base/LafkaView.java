package bg.devlabs.dev_lafka.ui.base;

import android.os.Handler;
import android.support.annotation.StringRes;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public interface LafkaView {

	/**
	 * Removed the user's expired token
	 */
	void openActivityOnTokenExpire();

	/**
	 * Shows an error with provided ID
	 * @param resId the ID provided
	 */
	void onError(@StringRes int resId);

	/**
	 * Shows an error with provided message
	 * @param message the message provided
	 */
	void onError(String message);

	/**
	 * Checks network state
	 * @return a boolean
	 */
	boolean isNetworkConnected();

	/**
	 * Hides Keyboard
	 */
	void hideKeyboard();

	/**
	 * 	Requests permission. Used (currently) only for camera permission
	 * @param permission Permission's string
	 * @param callback Handles user's aciton
	 */
	void requestPermission(String permission, Handler.Callback callback);


}
