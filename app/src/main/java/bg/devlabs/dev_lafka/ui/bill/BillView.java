package bg.devlabs.dev_lafka.ui.bill;

import java.util.List;

import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.ui.base.LafkaView;

/**
 * Created by Radoslav on 15-Mar-17.
 */

public interface BillView extends LafkaView {
	/**
	 * Fills the list with orders
	 * @param orders Object with user's orders
	 */
	void showList(List<OrdersResponse> orders);

	/**
	 * Shows an error?
	 */
	void showError();

	/**
	 * Deletes an item from the list based on position
	 * @param position The position
	 */
	void onItemDeleted(int position);

	/**
	 * Selects a product to send back to the Home fragment
	 * @param barcode the barcode of the item
	 */
	void selectProduct(String barcode);

	/**
	 * Fills the list with products
	 * @param products Object with lafka's products
	 */

	void showBuyList(List<ProductsResponse> products);
}
