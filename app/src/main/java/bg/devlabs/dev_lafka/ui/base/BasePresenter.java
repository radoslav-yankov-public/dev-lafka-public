package bg.devlabs.dev_lafka.ui.base;

import android.util.Log;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.utils.ApiError;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public class BasePresenter<V extends LafkaView> implements LafkaPresenter<V> {

	private static final String TAG = "RoundedImageView";

	private final DataManager mDataManager;

	private final CompositeDisposable mCompositeDisposable;

	private V mLafkaView;

	@Inject
	public BasePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
		this.mDataManager = dataManager;
		this.mCompositeDisposable = compositeDisposable;
	}


	@Override
	public void onAttach(V LafkaView) {
		mLafkaView = LafkaView;
	}

	@Override
	public void onDetach() {
		mCompositeDisposable.dispose();
		mLafkaView = null;

	}

	public boolean isViewAttached() {
		return mLafkaView != null;
	}

	public V getLafkaView(){
		return mLafkaView;
	}

	public void checkViewAttached() {
		if (!isViewAttached()) throw new MvpViewNotAttachedException();
	}

	public DataManager getDataManager() {
		return mDataManager;
	}

	/**
	 * 	Handling API errors
	 * @param error the API error
	 */
	@Override
	public void handleApiError(ANError error) {
		if (error == null || error.getErrorBody() == null) {
			getLafkaView().onError(R.string.api_default_error);
			return;
		}

		if (error.getErrorCode() == 0
				&& error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
			getLafkaView().onError(R.string.connection_error);
			return;
		}

		final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = builder.create();

		try {
			ApiError apiError = gson.fromJson(error.getErrorBody(), ApiError.class);

			if (apiError == null || apiError.getMessage() == null) {
				getLafkaView().onError(R.string.api_default_error);
				return;
			}

			switch (error.getErrorCode()) {
				case HttpsURLConnection.HTTP_UNAUTHORIZED:
				case HttpsURLConnection.HTTP_FORBIDDEN:
					setUserAsLoggedOut();
					getLafkaView().openActivityOnTokenExpire();
				case HttpsURLConnection.HTTP_INTERNAL_ERROR:
				case HttpsURLConnection.HTTP_NOT_FOUND:
				default:
					getLafkaView().onError(apiError.getMessage());
			}
		} catch (JsonSyntaxException | NullPointerException e) {
			Log.e(TAG, "handleApiError", e);
			getLafkaView().onError(R.string.api_default_error);
		}
	}

	/**
	 * Sets the user as logged off
	 */
	@Override
	public void setUserAsLoggedOut() {
		getDataManager().setAccessToken(null);
	}

	public static class MvpViewNotAttachedException extends RuntimeException {
		public MvpViewNotAttachedException() {
			super("Please call Presenter.onAttach(MvpView) before" +
					" requesting data to the Presenter");
		}
	}
}
