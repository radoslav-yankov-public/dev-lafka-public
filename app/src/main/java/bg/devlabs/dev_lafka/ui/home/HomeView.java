package bg.devlabs.dev_lafka.ui.home;

import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.ui.base.LafkaView;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public interface HomeView extends LafkaView {
	/**
	 * Shows an error on product buy attempt
	 */
	void showError();
	/**
	 * Shows a success on product buy attempt
	 */
	void showSuccess();

	/**
	 * Sets a selected item
	 * @param seleted the object of the selected item
	 */
	void setSelected(ProductsResponse seleted);
	void openHomeScreen();

	/**
	 * Shows a toast on shake
	 * @param Name Toast's message
	 */
	void onShake(String Name);

	/**
	 * Toggles the camera view for barcode scan
	 */
	void toggleCamera();

	/**
	 * Update's the user's bill
	 * @param bill The bill
	 */
	void updateBill(double bill);

	/**
	 * Automatically shows the bill
	 */
	void updateAutomatic();

	/**
	 * Shows all views after the card, with a slight fade after animation
	 */
	void showViews();

	/**
	 * Opens the screen with an already selected item
	 * @param id the items ID
	 */
	void openWithSelected(String id);

	/**
	 * Sets up icon shortcut
	 * @param text text on shortcut
	 */
	void setupIconShortcut(String text);
}
