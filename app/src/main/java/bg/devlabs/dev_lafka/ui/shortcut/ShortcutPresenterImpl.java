package bg.devlabs.dev_lafka.ui.shortcut;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.ui.base.BasePresenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Radoslav on 17-Mar-17.
 */

public class ShortcutPresenterImpl<V extends ShortcutView> extends BasePresenter<V> implements ShortcutPresenter<V> {

	@Inject
	public ShortcutPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
		super(dataManager, compositeDisposable);
	}

	@Override
	public void makeCall() {
		getDataManager().doMakeCall(getDataManager().getAccessToken(), Integer.parseInt(getDataManager().getLastItem()), 1)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(makeResponseApiResponse -> getLafkaView().showToast(makeResponseApiResponse.isSuccess()), throwable -> getLafkaView().showToast(false));
	}
}
