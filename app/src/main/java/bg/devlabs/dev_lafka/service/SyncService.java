package bg.devlabs.dev_lafka.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.di.component.DaggerServiceComponent;
import bg.devlabs.dev_lafka.di.component.ServiceComponent;

/**
 * Created by Radoslav on 06-Mar-17.
 */


public class SyncService extends Service {

	@Inject
	DataManager mDataManager;

	public static Intent getStartIntent(Context context) {
		return new Intent(context, SyncService.class);
	}

	public static void start(Context context) {
		Intent starter = new Intent(context, SyncService.class);
		context.startService(starter);
	}

	public static void stop(Context context) {
		context.stopService(new Intent(context, SyncService.class));
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ServiceComponent component = DaggerServiceComponent.builder()
				.applicationComponent(((Devlafka) getApplication()).getComponent())
				.build();
		component.inject(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
